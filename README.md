*If you're looking for something serious... it's probably not there*

*I may be gone from the Zoo, but my stupidity is still there*

![WBooster logo](./icon_large.svg)

# The Future of Productivity

WBooster (Work Booster) is an app created to enhance productivity, using new state of the art tricks discovered after years of research.

It consists of a clutter-free interface to listen *Radio Bullshit*, a special audio playlist with efficiency and productivity in mind.

If you're a programmer, You can also check WBooster sister app : [WBooster VSCode Edition](https://gitlab.com/Aethor/wbooster-vscode-edition)


# Installation

> git clone https://gitlab.com/Aethor/wbooster.git

you need the following python dependencies :

* vlc (pip3 install python-vlc)
* youtube_dl (pip3 install youtube_dl)
* PyGobject (see [here](https://pygobject.readthedocs.io/en/latest/getting_started.html) and don't complain to me that it's hard to install or something)

you can install all dependencies using :
> pip3 install -r requirements.txt

If it doesn't work, it doesn't work.


# Usage

launch the main file with :

> python3 wbooster.py

If this is the first lauch, wbooster will download informations from the playlist. Afterwards, the first song will launch.

The interface is simple and streamlined, encouraging efficient workflow. The only exposed control mechanism is the "Play / Pause" button, allowing for a complexity-free experience.


# Extensions

WBooster supports extensions thanks to its ingenious design. When first launching a plugin, it will first download it from git in the *plugins* directory.

* The e-booster extension lets you enhance your hearing by slightly modifying the original song (see https://gitlab.com/Clodion/e-booster). Use it with the `-e` flag (`python3 wbooster.py -e` or `python3 wbooster.py --e-booster`). Please note you need *librosa* installed on your system first (`pip install librosa`)


# Special thanks

Thanks to Zer for designing the Awesome WBooster logo !


# FAQ


## It's slow and unefficient, you could have buffered audio files or something

I could have done that. But yeah, maybe I'll do it.


## I found a bug

Yes


## It's completely broken wtf

Yes


## I want to complain. Where can I complain ?

To prevent a lowering of your productivity, no way of contacting me will be described


# ISO certifications

This project is certified [ISO-1664](https://www.la-rache.com/)

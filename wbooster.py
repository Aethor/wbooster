#!/usr/bin/python

import gi

gi.require_version("Gtk", "3.0")
gi.require_version("Notify", "0.7")
from gi.repository import Gtk, GLib, Notify

import youtube_dl
import vlc

from typing import Set
import os
import random
import json
import argparse
from utils import globalize_path


playlist_infos_filename = globalize_path("infos.json")
download_dir = globalize_path("songs/")


class WBooster(object):
    def __init__(self, plugins: Set[str]):
        self.plugins = plugins
        for plugin in self.plugins:
            print(f"Using {plugin} plugin")

        self.builder = Gtk.Builder()
        self.builder.add_from_file(globalize_path("window.glade"))

        self.window = self.builder.get_object("window")
        self.window.set_icon_from_file(globalize_path("icon.svg"))
        self.window.connect("destroy", Gtk.main_quit)
        self.window.set_title("WBooster -- Radio Bullshit")

        self.title_label = self.builder.get_object("title_label")
        self.title_label_offset = 0
        self.progression_slider = self.builder.get_object("progression_slider")

        self.play = True
        self.selected_video = None
        self.media_player = None
        self.play_button = self.builder.get_object("play_button")
        self.play_button.connect("clicked", self.toggle_play)
        GLib.timeout_add_seconds(1, self.check_player_status_callback)
        GLib.timeout_add(100, self.title_label_refresh_callback)

        self.playlist_infos = self.get_playlist_infos()

        self.window.show_all()

    def title_label_refresh_callback(self):
        if self.media_player is None:
            return True

        if self.media_player.get_state() == vlc.State.Playing:
            if self.title_label_offset < len(self.selected_video["title"]) * 2:
                self.title_label_offset += 1
            else:
                self.title_label_offset = 0
            stupid_str = (
                " " * len(self.selected_video["title"])
                + self.selected_video["title"]
                + " " * len(self.selected_video["title"])
            )
            self.title_label.set_text(
                stupid_str[
                    self.title_label_offset : self.title_label_offset
                    + len(self.selected_video["title"])
                ]
            )

        return True

    def check_player_status_callback(self):
        if (
            self.media_player is None
            or self.media_player.get_state() == vlc.State.Ended
        ):
            self.play_random_video()
        if self.media_player.get_state() == vlc.State.Playing:
            self.progression_slider.set_value(
                self.media_player.get_position()
                * self.progression_slider.get_adjustment().get_upper()
            )

        return True

    def get_playlist_infos(self):
        if os.path.exists(playlist_infos_filename):
            with open(playlist_infos_filename, "r") as f:
                return json.load(f)

        ydl = youtube_dl.YoutubeDL(params={"ignoreerrors": True})
        infos = ydl.extract_info(
            "https://www.youtube.com/playlist?list=PLQC73oq6JtgyYNOeifrJXXzZ1-F0Kgmbg",
            download=False,
        )
        with open(playlist_infos_filename, "w") as f:
            json.dump(infos, f)
        return infos

    def toggle_play(self, button):
        self.play = not self.play
        self.play_button.set_label("⏸" if self.play else "▶")

        if not self.play and not self.media_player is None:
            self.media_player.pause()

        if self.selected_video is None and self.play:
            self.play_random_video()
        elif self.play:
            self.media_player.play()

    def play_random_video(self):
        if not self.media_player is None:
            self.media_player.stop()

        for attempt in range(20):
            try:
                self.selected_video = random.choice(self.playlist_infos["entries"])
                url = "https://www.youtube.com/watch?v=" + self.selected_video["id"]

                if "e-booster" in self.plugins:
                    import importlib

                    stupid_extension = ".godwin"
                    ebooster = importlib.import_module("plugins.e-booster.e-booster")
                    filename = ebooster.download_wav(
                        url, download_dir + "%(title)s" + stupid_extension
                    )
                    filename = (
                        filename[: len(filename) - len(stupid_extension)] + ".wav"
                    )
                    ebooster.audio2earrape(filename, filename)
                    print("e-booster : ear-rape successfull")
                else:
                    ydl = youtube_dl.YoutubeDL(
                        {
                            "format": "bestaudio/best",
                            "outtmpl": download_dir + "%(title)s.godwin",
                            "restrictfilenames": True,
                        }
                    )
                    infos = ydl.extract_info(url, download=True)
                    filename = ydl.prepare_filename(infos)
            except Exception as e:
                print("[wbooster] could not download video - trying another one...")
            else:
                break
        else:
            print("[wbooster] too much fail while downloading videos - exitting...")
            exit()

        self.media_player = vlc.MediaPlayer(filename)
        self.media_player.play()
        self.title_label.set_text(self.selected_video["title"])
        print("[wbooster] now playing " + self.selected_video["title"])
        Notify.init("[wbooster]")
        Notify.Notification.new(
            "[wbooster]",
            "now playing : " + self.selected_video["title"],
            "dialog-information",
        ).show()


parser = argparse.ArgumentParser()
parser.add_argument(
    "-e",
    "--e-booster",
    action="store_true",
    default=None,
    help="activate e-booster plugin. necessitate librosa",
)
args = parser.parse_args()

plugins = set()

if not args.e_booster is None:
    if not os.path.isdir("./plugins/e-booster"):
        os.system("cd ./plugins;git clone https://gitlab.com/Clodion/e-booster.git")
    plugins.add("e-booster")

WBooster(plugins)
Gtk.main()

import os

def globalize_path(path: str) -> str:
    if path.startswith("./"):
        path = path[2:]
    return os.path.dirname(os.path.abspath(__file__)) + "/" + path
